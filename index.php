<?php
include_once('config.php');
error_reporting(E_ALL);

function neologiere($length) {
    global $pdo;
    $neologismus = '';
    $statement = $pdo->prepare('SELECT syllable, length FROM syllables WHERE length <= ? ORDER BY RAND() LIMIT 1');
    while(strlen($neologismus) < $length) {
        $statement->execute(array($length-strlen($neologismus)));
        $neologismus .= $statement->fetch()['syllable'];;
    }
    return $neologismus;
}

try {
    $pdo = new PDO(PDO_CONNECTION, DATABASE_USER, DATABASE_PASSWORT);
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $length = empty($_GET['length']) ? 4 : (int)$_GET['length'];
    $num = empty($_GET['number']) ? 1 : (int)$_GET['number'];

    for($i=0; $i<$num; $i++) {
        echo neologiere($length)."\n";
    }

} catch(PDOException $e) {
    echo 'Connection to database failed: ' . $e->getMessage();
}