<?php
/*
 * File: fill_database.php
 * Description: This file's sole purpose is to fill the database with syllable data to mirrow the dlexDB (dlexdb.de)
 *
 */

include_once('config.php');

try {
    $pdo = new PDO(PDO_CONNECTION, DATABASE_USER, DATABASE_PASSWORT);
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->query('CREATE TABLE IF NOT EXISTS syllables (id INT AUTO_INCREMENT PRIMARY KEY, syllable TEXT, length INT)');

    $data = file(DATA_PATH, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach($data as $syllable) {
        $stmt = $pdo->prepare('INSERT INTO syllables VALUES (NULL, ?, ?)');
        $stmt->execute(array($syllable, strlen($syllable)));
    }


} catch(PDOException $e) {
    echo 'Connection to database failed: ' . $e->getMessage();
}