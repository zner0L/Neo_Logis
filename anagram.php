<?php
function anagram($word, $length) {
    $word = str_split($word);
    shuffle($word);
    $anagram = '';
    $i=0;
    while(strlen($anagram) < $length) {
        if($i<count($word)) {
            $anagram .= $word[$i];
            $i++;
        } else {
            shuffle($word);
            $i=0;
        }
    }

    return $anagram;
}

$num = empty($_GET['number']) ? 1 : (int)$_GET['number'];

for($i=0; $i<$num; $i++) {
    echo anagram($_GET['word'], (int)$_GET['length']) . "\n";
}

